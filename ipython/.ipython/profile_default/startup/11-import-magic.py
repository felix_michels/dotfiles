import sys
import os
import re


class __LazyLoader:

    def __init__(self, module_name, alias, object_name=None):
        self._LazyLoader_module_name = module_name
        self._LazyLoader_object_name = object_name
        self._LazyLoader_alias = alias

        self._LazyLoader_object = None  # Holds the actual module or object once loaded
        self._LazyLoader_done_loading = False


    def _LazyLoader_set_object(self):
        if not self._LazyLoader_done_loading:
            self._LazyLoader_done_loading = True
            try:
                import importlib
                self._LazyLoader_object = importlib.import_module(self._LazyLoader_module_name)
                if self._LazyLoader_object_name is not None:
                    self._LazyLoader_object = getattr(self._LazyLoader_object, self._LazyLoader_object_name)
            except (ImportError, AttributeError) as e:
                if globals()[self._LazyLoader_alias] is self:
                    del globals()[self._LazyLoader_alias]
                raise e
            globals()[self._LazyLoader_alias] = self._LazyLoader_object

    def __getattribute__(self, name):
        if name in (
                '_LazyLoader_module_name',
                '_LazyLoader_object_name',
                '_LazyLoader_alias',
                '_LazyLoader_object',
                '_LazyLoader_module_name',
                '_LazyLoader_set_object',
                '_LazyLoader_done_loading'):
            return super().__getattribute__(name)

        self._LazyLoader_set_object()
        return getattr(self._LazyLoader_object, name)


class __LazyLoaderCallable(__LazyLoader):
    def __call__(self, *args, **kwargs):
        # function call obj(...) bypasses __getattribute__
        self._LazyLoader_set_object()
        return self._LazyLoader_object(*args, **kwargs)



def __parse_import_line(import_str):
    result = []
    import_str = import_str.strip()
    if len(import_str) == 0:
        return []

    # Handle 'from ... import ...' case
    import_match = re.match(r"from\s+(\S+)\s+import\s+(.+)", import_str)
    if import_match:
        module_name = import_match[1]
        objects = import_match[2].split(",")
        for obj in objects:
            obj = obj.strip()
            alias_match = re.match(r"(\S+)\s+as\s+(\S+)", obj)
            if alias_match:
                result.append({
                    'module_name': module_name,
                    'object_name': alias_match[1],
                    'alias': alias_match[2]
                })
            else:
                result.append({
                    'module_name': module_name,
                    'object_name': obj,
                    'alias': obj
                })
        return result

    # Handle 'import ...' case
    import_match = re.match(r"import\s+(\S.*)", import_str)
    if import_match:
        modules = import_match[1].split(",")
        for module in modules:
            module = module.strip()
            alias_match = re.match(r"(\S+)\s+as\s+(\S+)", module)
            if alias_match:
                result.append({
                    'module_name': alias_match[1],
                    'object_name': None,
                    'alias': alias_match[2]
                })
            else:
                result.append({
                    'module_name': module,
                    'object_name': None,
                    'alias': module
                })

        return result
    raise ValueError(f"Invalid import statement: {import_str}")


def __do_lazy_import(statements):
    assert not __do_lazy_import._executed, "`__do_lazy_import` may only be called once"
    def lazy_import(line):
        for kwargs in __parse_import_line(line):
            init_fn = __LazyLoaderCallable if kwargs['object_name'] else __LazyLoader
            globals()[kwargs['alias']] = init_fn(**kwargs)


    from IPython.core.magic import register_line_magic
    @register_line_magic
    def lazy(line):
        line = line.strip()
        if line == 'off':
            glbs = list(globals())
            for alias in glbs:
                if isinstance(globals()[alias], __LazyLoader):
                    del globals()[alias]
        elif line == 'on':
            for import_line in statements.splitlines():
                lazy_import(import_line)
        else:
            lazy_import(line)
    lazy('on')
    __do_lazy_import._executed = True
__do_lazy_import._executed = False

__do_lazy_import(_LAZY_IMPORTS)
