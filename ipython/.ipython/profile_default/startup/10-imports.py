_LAZY_IMPORTS = """
    from collections import defaultdict
    from pathlib import Path
    from pprint import pprint, pformat

    import numpy as np
    import torch
    import torch.nn as nn
    import torch.nn.functional as F
    from torch.utils.data import DataLoader

    import torchvision
    import torchvision.datasets as datasets
    import torchvision.transforms as T

    from safetensors import safe_open
    from safetensors.torch import save_file
    from safetensors.numpy import save_file as np_save_file

    from tqdm.autonotebook import tqdm, trange
    import pandas as pd
    import matplotlib.pyplot as plt
    import polars as pl
    """
