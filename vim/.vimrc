set nocompatible

syntax on
filetype indent on
filetype plugin on

call plug#begin('~/.vim/plugged')
Plug 'ctrlvim/ctrlp.vim' " Fuzzy buffer/files/mru search
Plug 'scrooloose/nerdtree' " directory viewer
Plug 'Shougo/neocomplete.vim' " autocomplete with cashe
Plug 'easymotion/vim-easymotion' " Movement stuff
Plug 'bling/vim-airline' " Pretty Status line
Plug 'Shougo/neosnippet' | Plug 'Shougo/neosnippet-snippets'
call plug#end()
let g:neocomplete#enable_at_startup = 1 " Enable neocomplete
let g:ctrlp_cmd = 'CtrlPBuffer' " Default to buffer search in ctrlp
let g:airline#extensions#tabline#enabled = 1 " Show open buffers on top

"appearance"
set background=dark
colorscheme solarized
set wildmenu
set number
set ruler
set nowrap
set listchars=tab:>-,trail:~,space:۰
set laststatus=2 " Always show statusline; default in neovim

"tabs"
set backspace=2
set tabstop=4
set softtabstop=0
set shiftwidth=0
set noexpandtab

"behavior"
set incsearch
set mouse=a
set hidden

"key bindings"
nnoremap <leader>ev :e $MYVIMRC <cr>
nnoremap <leader>sv :source $MYVIMRC <cr>

cnoremap w!! w !sudo tee % > /dev/null
noremap <F2> :mksession! ~/.vim_session <cr>
noremap <F3> :source ~/.vim_session <cr>
noremap <F5> :make <cr>
nnoremap <Space> i_<Esc>r
noremap <silent> <F6> :copen <cr>
noremap <silent> <F7> :cclose <cr>
nnoremap <leader>w :w <cr>
" viewable stuff
noremap <silent> <F9> :setl list! <cr>
nnoremap <silent> <leader>h :nohl <cr>
" Easier split movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap <C-h> <C-w>h
" Buffer stuff
nnoremap <leader>t :bn <cr>
nnoremap <leader>r :bp <cr>
nnoremap <leader>d :bd\|bd # <cr>
nnoremap <leader>g :CtrlPTag <cr>
" Snippet
imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)
xmap <C-k> <Plug>(neosnippet_expand_or_jump)

set splitright
set splitbelow
set switchbuf=useopen,usetab
