#!/bin/bash

set -e

lockfile="$HOME/.local/dot-setup.lock"
required=("curl" "zsh" "nvim" "stow")

if [[ -f "$lockfile" ]]; then
	echo "Lockfile exists. Exiting"
	exit 1
fi

for program in "${required[@]}"; do
	if [[ ! -x $(command -v "$program") ]]; then
		echo "Error: $program is not installed. Exiting."
		exit 1
	fi
done

mkdir -p $(dirname "$lockfile")
touch "$lockfile"

curl -LfsS "https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh" -o "/tmp/omz-install.sh"
sh "/tmp/omz-install.sh" --unattended
# set theme and plugins
echo "Installed Oh My Zsh" | tee -a "$lockfile"


curl -fsSL https://pixi.sh/install.sh | bash
export PATH="~/.pixi/bin:$PATH"
pixi global install --environment default --expose python --expose jupyter --expose ipython python ipython jupyter numpy pandas polars pynvim matplotlib

curl -LsSf https://astral.sh/uv/install.sh | sh

echo "Finished installing pixi & uv" | tee -a "$lockfile"

echo "Setting up neovim"
curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
nvim +PlugInstall +qall
nvim +UpdateRemotePlugins +qall # for deoplete
echo "Finished setting up neovim" | tee -a "$lockfile"

stow --verbose --no-folding zsh
stow --verbose --no-folding nvim
stow --verbose --no-folding git
stow --verbose --no-folding tmux
stow --verbose --no-folding ipython
stow --verbose --no-folding scripts
echo "Stowed dotfiles" | tee -a "$lockfile"

zsh -ic "omz theme set fino-conda"
zsh -ic "omz plugin disable git"
zsh -ic "omz plugin enable fzf colored-man-pages aliases zsh-interactive-cd"

# Custom plugins
zsh -ic "omzplug install TamCore/autoupdate-oh-my-zsh-plugins autoupdate" # Needs to be named autoupdate

zsh -ic "omz plugin enable autoupdate"
echo "Set omz settings" | tee -a "$lockfile"
