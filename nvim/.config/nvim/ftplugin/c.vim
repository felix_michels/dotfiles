if empty(glob("Makefile"))
    setlocal makeprg=gcq
    noremap <buffer> <F5> :w\|Neomake <cr>
endif
