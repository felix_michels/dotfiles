syntax on
filetype indent on
filetype plugin on

" Install vim-plug from https://github.com/junegunn/vim-plug
call plug#begin('~/.config/nvim/plugged')
Plug 'ctrlpvim/ctrlp.vim' " Fuzzy buffer/files/mru search
Plug 'scrooloose/nerdtree' " directory viewer
Plug 'Shougo/deoplete.nvim' " Async autocomplete
Plug 'benekastah/neomake' " Async make
Plug 'easymotion/vim-easymotion' " Movement stuff
Plug 'vim-airline/vim-airline' " Pretty Status line
Plug 'vim-airline/vim-airline-themes' " Airline themes
Plug 'Shougo/neosnippet' | Plug 'Shougo/neosnippet-snippets'
Plug 'scrooloose/nerdcommenter'
Plug 'neovimhaskell/haskell-vim'
Plug 'rust-lang/rust.vim'
Plug 'dense-analysis/ale' " Linting and completion
Plug 'easymotion/vim-easymotion'
call plug#end()
let g:python3_host_prog='~/.pixi/bin/python'
let g:deoplete#enable_at_startup = 1 " Enable deoplete
"let g:ctrlp_cmd = 'CtrlPBuffer' " Default to buffer search in ctrlp
let g:airline#extensions#tabline#enabled = 1 " Show open buffers on top
let g:neosnippet#snippets_directory = '$HOME/.config/nvim/snippets/'

let g:ale_python_auto_virtualenv = 1
let g:ale_python_flake8_options = '--max-line-length=99'
let g:ale_python_pylint_options = '--disable=C0114,C0115,C0116' " ignore missing docstring

let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`

" appearance
set background=dark
colorscheme PaperColor
set wildmenu
set number
set ruler
set nowrap
set listchars=tab:>-,trail:~,space:۰
set nohlsearch

" tabs
set backspace=2
set tabstop=4
set softtabstop=0
set shiftwidth=0
set noexpandtab

" behavior
set incsearch
set mouse=a
set complete+=i
set hidden
set backup
set backupdir=~/.local/share/nvim/backup

" filetypes
au BufRead,BufNewFile *.service	setlocal syntax=gitconfig
au BufRead,BufNewFile *.timer	setlocal syntax=gitconfig
au BufRead,BufNewFile *.pl		setlocal filetype=prolog

"Prosa
au BufEnter * let b:prosa_mode=0

function ToggleProsa()
	if b:prosa_mode==0
		setl spell
		setl spelllang=de
		setl tw=90
		setl expandtab
		let b:prosa_mode=1
		echo "Prosa mode on"
	else
		setl nospell
		setl spelllang=en
		setl tw=0
		setl noexpandtab
		let b:prosa_mode=0
		echo "Prosa mode off"
	endif
endfunction

map <silent> <M-p> :call ToggleProsa()<cr>

" key bindings
nnoremap Ö :

nnoremap <leader>ev :e $MYVIMRC <cr>
nnoremap <leader>sv :source $MYVIMRC <cr>

cnoremap w!! w !sudo tee % > /dev/null
noremap <F2> :mksession! ~/.local/share/nvim/sessions/nvim_session <cr>
noremap <F3> :source ~/.local/share/nvim/sessions/nvim_session <cr>
noremap <F5> :w\|Neomake! <cr>
nnoremap <Space> i_<Esc>r
noremap <silent> <F6> :copen <cr>
noremap <silent> <F7> :cclose <cr>
noremap <leader>w :w <cr>
tnoremap <Esc> <C-\><C-n>
" viewable stuff
noremap <silent> <F9> :setl list! <cr>
noremap <silent> <leader>h :nohl <cr>
" Easier split movement
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" Buffer stuff
nnoremap <leader>t :bn <cr>
nnoremap <leader>r :bp <cr>
nnoremap <leader>d :bp\|bd # <cr>
nnoremap <leader>g :CtrlPTag <cr>
" Snippet
imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)
xmap <C-k> <Plug>(neosnippet_expand_target)

set splitright
set splitbelow
set switchbuf=useopen,usetab
