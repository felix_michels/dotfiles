tsh () {
	ssh -t "$1" "tmux new-session -ADs main"
}

ssht () {
	ssh -N -L "$2":localhost:"$2" "$1"
}
