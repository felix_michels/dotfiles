cuenv() {
	CUDA_VISIBLE_DEVICES="$1" command "${@:2}"
}

cupy() {
	CUDA_VISIBLE_DEVICES="$1" python "${@:2}"
}

cuddp() {
	num_dev=$(echo "$1" | tr ',' ' ' | wc -w)
	CUDA_VISIBLE_DEVICES="$1" torchrun --standalone --nproc-per-node="$num_dev" "${@:2}"
}

gpustat_custom() {
    if [[ "$@" == *"-i"* || "$@" == *"--interval"* || "$@" == *"--watch"* ]]; then
        # Pass through without sed for interactive modes
        gpustat "$@"
    else
        # Change bold-black so that it's visible with dark color scheme
        gpustat --force-color "$@" | sed 's/\x1b\[1m\x1b\[30m/\x1b[36m/g'
    fi
}
alias gpustat="gpustat_custom"
