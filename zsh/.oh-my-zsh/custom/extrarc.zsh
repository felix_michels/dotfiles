alias stow="stow --verbose --no-folding --ignore='(\.mypy/.*|.*\.unison|.*\.unison\.tmp)' --dir=$HOME/dotfiles"
alias rr=". ranger"
alias jns='uv run --with jupyter --with pickleshare jupyter lab --no-browser'
alias ipy='uv run --with ipython --with pickleshare ipython'

if [[ "$(uname)" == "Linux" ]]; then
	alias sctl='systemctl'
	alias ssctl='sudo systemctl'
	alias uctl='systemctl --user'
	alias rsync="rsync -h --info=progress2"

	alias cp='cp --reflink=auto'
fi

if command -v nvim &>/dev/null; then
	alias vim=nvim
fi

if command -v gh &>/dev/null && [[ -x "$HOME/.local/share/gh/extensions/gh-copilot/gh-copilot" ]]; then
	eval "$(gh copilot alias -- zsh)"
fi

if command -v uv &>/dev/null; then
	eval "$(uv generate-shell-completion zsh)"
fi

if command -v pixi &>/dev/null; then
	eval "$(pixi completion --shell zsh)"
fi

_omzplug::install() {
  if [[ $# -lt 1 || $# -gt 2 ]]; then
    echo "Usage: omzplug install author/package [name]" >&2
    return 1
  fi

  local author_package="$1"
  local author="${author_package%%/*}"
  local package="${author_package##*/}"
  local name="${2:-$package}"
  local dest="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/$name"

  git clone "https://github.com/$author/$package.git" "$dest" --depth=1
  omz plugin enable "$name"
}

_omzplug::remove() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: omzplug remove name" >&2
    return 1
  fi
  local name="$1"
  local dest="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/$name"
  echo "Removing $dest..."
  rm -rf "$dest"
  omz plugin disable "$name"
}


_omzplug::search() {
    local query="${*// /+}"  # Join all arguments and replace spaces with '+'
	local num_results=10
    curl -s "https://api.github.com/search/repositories?q=$query&order=desc&per_page=$num_results" \
		| jq -r '.items[].full_name'
}

omzplug() {
  local subcommand="$1"
  shift

  case "$subcommand" in
    "install")
      _omzplug::install "$@"
      ;;
    "remove")
      _omzplug::remove "$@"
      ;;
    "search")
      _omzplug::search "$@"
      ;;
    *)
      echo "Error: Unknown subcommand '$subcommand'" >&2
      return 1
      ;;
  esac
}

